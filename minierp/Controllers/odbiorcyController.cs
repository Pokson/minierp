﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using minierp.Models;

namespace minierp.Controllers
{
    public class odbiorcyController : Controller
    {
        private odbiorcy db = new odbiorcy();

        // GET: odbiorcy
        public ActionResult Index()
        {
            return View(db.odbiorca.ToList());
        }

        // GET: odbiorcy/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odbiorca odbiorca = db.odbiorca.Find(id);
            if (odbiorca == null)
            {
                return HttpNotFound();
            }
            return View(odbiorca);
        }

        // GET: odbiorcy/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: odbiorcy/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "odbiorca_id,nazwa,powiat,gmina,kodpoczt,miasto,poczta,ulica,nrdomu,rabat")] odbiorca odbiorca)
        {
            if (ModelState.IsValid)
            {
                db.odbiorca.Add(odbiorca);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(odbiorca);
        }

        // GET: odbiorcy/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odbiorca odbiorca = db.odbiorca.Find(id);
            if (odbiorca == null)
            {
                return HttpNotFound();
            }
            return View(odbiorca);
        }

        // POST: odbiorcy/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "odbiorca_id,nazwa,powiat,gmina,kodpoczt,miasto,poczta,ulica,nrdomu,rabat")] odbiorca odbiorca)
        {
            if (ModelState.IsValid)
            {
                db.Entry(odbiorca).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(odbiorca);
        }

        // GET: odbiorcy/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odbiorca odbiorca = db.odbiorca.Find(id);
            if (odbiorca == null)
            {
                return HttpNotFound();
            }
            return View(odbiorca);
        }

        // POST: odbiorcy/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            odbiorca odbiorca = db.odbiorca.Find(id);
            db.odbiorca.Remove(odbiorca);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
